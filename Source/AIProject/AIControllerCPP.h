// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/BehaviorTreeComponent.h"
#include "CoreMinimal.h"
#include "AIController.h"
#include "AIControllerCPP.generated.h"

/**
 * 
 */
UCLASS()
class AIPROJECT_API AAIControllerCPP : public AAIController
{
	GENERATED_BODY()

	UBehaviorTreeComponent* BehaviorComp;

	UBlackboardComponent* BlackBoardComp;

	//virtual void OnPossess(APawn* Pawn) override;
	
};
